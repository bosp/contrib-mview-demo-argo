
ifdef CONFIG_CONTRIB_MVIEW_DEMO_ARGO

# Targets provided by this project
.PHONY: mview-demo-argo clean_mview-demo-argo

# Add this to the "contrib_testing" target
contrib: mview-demo-argo
clean_contrib: clean_mview-demo-argo

MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO=contrib/user/mview-demo-argo


.cmd_ok:
	$(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/dataset/check_cmds.sh

mview-demo-argo: external .cmd_ok
	@echo
	@echo "==== Building OpenMP-MView-ARGO ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1
	@cp $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/start-mview-demo-argo $(BUILD_DIR)/usr/bin
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/dataset && \
		./install.sh || \
		exit 1
	@echo

clean_mview-demo-argo:
	@echo
	@echo "==== Clean-up OpenMP-MView-ARGO Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/ompstereomatch ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/MviewDemoArgo.recipe; \
		rm -f $(BUILD_DIR)/usr/bin/mview-demo-argo; \
		rm -f $(BUILD_DIR)/etc/bbque/MviewDemoArgo.conf; \
		rm -f $(BUILD_DIR)/usr/bin/start-mview-demo-argo; \
		rm -f $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/dataset/Aloe/i-out.bmp
	@rm -rf $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/build
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_ARGO)/dataset && \
		./uninstall.sh
	@echo

else # CONFIG_CONTRIB_MVIEW_DEMO_ARGO

mview-demo-argo:
	$(warning contrib module OpenMP-MView-ARGO disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_MVIEW_DEMO_ARGO

