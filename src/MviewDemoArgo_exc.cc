/**
 *       @file  MviewDemoArgo_exc.cc
 *      @brief  The OpenMP-MView-ARGO BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *              Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <chrono>

#include "MviewDemoArgo_exc.h"
#include "op_list.h"

#include <bbque/utils/utility.h>
#include <omp.h>


// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.OmpMview"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetChUid()



MviewEXC::MviewEXC(
		std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		ImgBufferPtr buffer,
		std::string const & log_file,
		int grayscale,
		float fps,
		int exc_id) :
	BbqueEXC(name, recipe, rtlib),
	buffer(buffer),
	exit_loop(false),
	grayscale(grayscale),
	fps(fps),
	exc_id(exc_id){

	logger->Info("New MviewEXC::MviewEXC()");
	logger->Info("EXC Unique IDentifier (UID): %u", GetUid());
	
	// open the log file
	file_logger.open(log_file, std::fstream::out | std::fstream::trunc);
	
	
	// write the header
	file_logger << "Timestamp, ";
	file_logger << "Throughput_profiled, ";
	file_logger << "Throughput_measured, ";
	file_logger << "Throughput_goal, ";
	file_logger << "Error_profiled, ";
	file_logger << "Error_measured, ";
	file_logger << "Error_goal, ";
	file_logger << "Thread_number, ";
	file_logger << "Cpu_usage, ";
	file_logger << "Cpu_available";
	file_logger << std::endl;
	
}

RTLIB_ExitCode_t MviewEXC::onSetup() {
	logger->Info("MviewEXC::onSetup()");

	// Read reference disparity map (gray level bitmap)
	buffer->getFirstReferenceFrame(outMap);
	
	// Get frame size
	int imgtype;
	buffer->getImageStat(imgwidth, imgheight, imgtype);

	// Initialize the input images
	bitmapLx = cv::Mat(imgheight,imgwidth,imgtype, cv::Scalar::all(0));
	bitmapRx = cv::Mat(imgheight,imgwidth,imgtype, cv::Scalar::all(0));
	
	// Init the proximity regions
	buffer->getProximityRegionStat(square_size, row_number, col_number);
	regions.resize(row_number);
	for (unsigned int i = 0; i < row_number; i++)
	{
		regions[i].resize(col_number, 0);
	}

	
	// Allocate EXC buffers
	size_t num_pixels = imgwidth * imgheight;

	crosses_lx    = (cross_t *)calloc(num_pixels, sizeof(cross_t));
	crosses_rx    = (cross_t *)calloc(num_pixels, sizeof(cross_t));
	raw_matchcost = (int *)calloc(num_pixels, sizeof(int));
	matchcost     = (int *)calloc(num_pixels, sizeof(int));
	anchorreg     = (int *)calloc(num_pixels, sizeof(int));
	fmatchcost    = (int *)calloc(num_pixels, sizeof(int));
	fanchorreg    = (int *)calloc(num_pixels, sizeof(int));
	dpr_votes     = (vote_t *)calloc(num_pixels, sizeof(vote_t));
	dpr_block     = (dpr_t *)calloc(num_pixels, sizeof(dpr_t));
	
	
	/* Setting the run-time optimization */
	using namespace argo::monitor;
	
	
	/*
	 * Create the monitor
	 * Is possible to set the number of element stored in the 
	 * observation window and used to extract statistical
	 * properties, such as average or variance
	 */
	#define WINDOW_SIZE 5
	throughput_monitor = ThroughputMonitorPtr( new ThroughputMonitor(WINDOW_SIZE) );
	
	
	
	/*
	 * Create the goal
	 * In order to create a goal is needed to specify:
	 *   - The monitor used to gather the data
	 *   - The statistical property object of the goal
	 *   - The kind of comparison of the goal
	 *   - The actual goal value
	 */
	throughput_goal = ThroughputGoalPtr( new ThroughputGoal( throughput_monitor,
		argo::DataFunction::Average,
		argo::ComparisonFunction::Greater,
		fps  ) );
	
	/*
	 * Application-Specific Run-Time Manager configuration
	 * 
	 * Step 1
	 * Creation of the asrtm, the throughput monitor and goal
	 * 
	 * Step 2
	 * Defining the optimization behaviour
	 */
	
	/* Create the AS-RTM */
	asrtm = argo::asrtm::AsrtmPtr( new argo::asrtm::Asrtm(opList) );

	/* Define the behaviour of the normal state */
	asrtm->changeState("Normal");	
	
	/* Set the rank
	 * There are available three type of ranking structore:
	 *  -> Simple rank: use only one metric or parameter to
	 *     define the rank value.
	 *     It's enough to specify the name of subject
	 * 
	 *  -> Linear rank: use a linear combination of metric or
	 *     parameter (field in general) to compute the rank value as:
	 *       c1*f1 + c2*f2 + ...
	 *     where c1,c2,... are the coefficients that define the
	 *     importance of the fields f1,f2,...
	 *     so the command is like:
	 *     setLinearRank( { {f1, c1}, {f2, c2}, ... }, argo::RankObjective )
	 *
	 *  -> Geometric rank: use a geometric combination of fields to
	 *     compute the rank value as:
	 *       (f1^c1)*(f2^c2)*...
	 *     so the command is like:
	 *     setGeometricRank( { {f1, c1}, {f2, c2}, ... }, argo::RankObjective )
	 * 
	 * Note: the rank function is a one-shot computation so it doesn't
	 * impact on the application performance
	 * 
	 * The second field tell to the asrtm if a good OP minimize or
	 * maximize the rank value.
	 */
	asrtm->setSimpleRank("norm_error", argo::RankObjective::Minimize);
	
	
	/* ######
	 * Set Application-Specific Constraints
	 * The application-specific constraint must be in a lower position
	 * with respect to the resource-specific constraints
	 * #####
	 *
	 *
	 * Set the throughput constraint
	 * 
	 * Since the throughput is monitored during run-time is possible to 
	 * express a dynamic constraint that explot the measuration
	 * gathered at Run-Time to adjust the profiled metrics
	 *
	 * To add a constraint is needed to specify:
	 *  - The goal object that holds the actual definition
	 *  - The name of the field in the OP definition
	 *  - The noise threshold (1). In percentage, the default value is 0
	 *  - The validity size (2). The default value is 1
	 *
	 * (1) The gathered data are likely to be affected to some kind on noise.
	 * If the goal value is close to the measure, may happen that the noise
	 * trigger a reconfiguration. In order to avoid this scenario, is possibile
	 * to set a threshold on the difference between the observed and profiled data.
	 * If that difference is below the threshold value, it will be ignored.
	 * NOTE: High threshold values may prevent the monitor to be used.
	 * 
	 * (2) The statistical properties may be significant only if they are
	 * computed using n values. This parameter is the minimum number of element
	 * that the monitor must have gathered in order to check the difference
	 * beetween the observed and profiled values.
	 * NOTE: If the parameter is greater than the size of the monitor, then 
	 * the information from the monitor are not used
	 */
#define NOISE_THRESHOLD  0.1f
#define VALIDITY_SIZE    2
	asrtm->addDynamicConstraintOnTop(throughput_goal, "throughput", NOISE_THRESHOLD, VALIDITY_SIZE);
	
	/*
	 * Set the error constraint
	 * 
	 * In this case the metric are monitored at Run-Time, but it's
	 * used only the profiled value
	 *
	 * To add a constraint is needed to specify:
	 *  - The name of the field in the OP definition
	 *  - The comparison type used in the constraint
	 *  - The actual value of the goal
	 */
#define ERROR_GOAL_VALUE 0.5f
	asrtm->addStaticConstraintOnTop("norm_error",
		argo::ComparisonFunction::Less, ERROR_GOAL_VALUE);
	
	
	/* ######
	 * Set Resource-Specific Constraints
	 * #####
	 *
	 *
	 * Since the value of the constraint is updated in the onConfigure()
	 * method, the value setted here is meaningless
	 */
	asrtm->addStaticConstraintOnTop("cpu_usage",
		argo::ComparisonFunction::LessOrEqual, res_constraints.back().cpu_usage);
	
	/* Set the behaviour of the Danger state */
	asrtm->changeState("Danger");

	/* Set the rank */
	asrtm->setSimpleRank("norm_error", argo::RankObjective::Minimize);

	/* Set the constraints */
	asrtm->addStaticConstraintOnTop("cpu_usage",
		argo::ComparisonFunction::LessOrEqual, res_constraints.back().cpu_usage);
	asrtm->addDynamicConstraintOnBottom(throughput_goal, "throughput", NOISE_THRESHOLD, VALIDITY_SIZE);
	asrtm->addStaticConstraintOnBottom("norm_error",
		argo::ComparisonFunction::Less, ERROR_GOAL_VALUE);


	/* Start with the normal state */
	asrtm->changeState("Normal");
	
	/*
	 * RTLib Cycles-Per-Second
	 */
	SetCPS(fps);
	
	return RTLIB_OK;
}



RTLIB_ExitCode_t MviewEXC::onConfigure(uint8_t awm_id) {

	logger->Warn("MviewEXC::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);

	// update the constraint on the resources
	asrtm->changeStaticConstraintGoalValue(0, res_constraints[awm_id].cpu_usage);

	// Set the initial parameters
	params = asrtm->getBestApplicationParameters();

	// copy the parameters value for performance sake
	max_hypo_value  = params.at("max_hypo_value");
	hypo_step       = params.at("hypo_step");
	max_arm_length  = params.at("max_arm_length");
	color_threshold = params.at("color_threshold");
	matchcost_limit = params.at("matchcost_limit");
	num_threads     = params.at("nthreads");

#ifdef _OPENMP
	// change the number of OpenMP threads
	if (num_threads > 0) {
		omp_set_num_threads(num_threads);
	}
#endif
	logger->Notice(" - OMP_NUM_THREADS ... %d", num_threads);
	logger->Notice(" - MAX_HYPO_VALUE .... %d", max_hypo_value);
	logger->Notice(" - HYPO_STEP ......... %d", hypo_step);
	logger->Notice(" - MAX_ARM_LENGTH .... %d", max_arm_length);
	logger->Notice(" - COLOR_THRESHOLD ... %d", color_threshold);
	logger->Notice(" - MATCHCOST_LIMIT ... %d", matchcost_limit);
	
	return RTLIB_OK;
}

RTLIB_ExitCode_t MviewEXC::onRun() {
	{
		std::lock_guard<std::mutex> lock(exit_mutex);

		if (exit_loop)
			return RTLIB_EXC_WORKLOAD_NONE;
	}	

	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	// Do one more cycle
	logger->Info("MviewEXC::onRun()      : EXC [%s]  @ AWM [%02d]",
		exc_name.c_str(), wmp.awm_id);
	
	
	// Get the input frames
	buffer->getInputImages(bitmapLx, bitmapRx);


	// Start the throughput monitor
	throughput_monitor->start();

#pragma omp parallel
{
	// Build support regions
	winBuild(crosses_lx, (const rgb_t *)bitmapLx.data);
	winBuild(crosses_rx, (const rgb_t *)bitmapRx.data);
	// Evaluate disparity hypotheses
	for (int d=0; d<=max_hypo_value; d+=hypo_step)
	{
		raw_horizontal_integral(
			(const rgb_t *)bitmapLx.data, (const rgb_t *)bitmapRx.data, d);
		horizontal_integral(d);
		vertical_integral(d);
		crossregion_integral(d);
	}
	// Refinement filter
	refinement((unsigned char *)outMap.data);
}
	// Stop the throughput monitor
	throughput_monitor->stop(1);  // it has elaborated one frame


	// Compute the proximity matrix
	for (unsigned int row = 0; row < row_number; row++)
	{
		for (unsigned int col = 0; col < col_number; col++)
		{
			regions[row][col] = get_square_distance(row, col);
		}
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t MviewEXC::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Info("MviewEXC::onMonitor()  : "
		"EXC [%s]  @ AWM [%02d], Cycle [%4d]",
		exc_name.c_str(), wmp.awm_id, Cycles());
	
	// write the run information
	file_logger << std::chrono::duration_cast<std::chrono::microseconds>
		(std::chrono::steady_clock::now().time_since_epoch()).count() << ", ";
	file_logger << asrtm->getMetricValue("throughput") << ", ";
	file_logger << throughput_monitor->get(argo::DataFunction::Average) << ", ";
	file_logger << throughput_goal->getGoalValue() << ", ";
	file_logger << asrtm->getMetricValue("norm_error") << ", ";
	file_logger << 0 << ", "; // currently not measured
	if (state == ApplicationState::Normal)
		file_logger << asrtm->getStaticConstraintValue(1) << ", ";
	else
		file_logger << asrtm->getStaticConstraintValue(2) << ", ";
	file_logger << params.at("nthreads") << ", ";
	file_logger << asrtm->getMetricValue("cpu_usage") << ", ";
	file_logger << res_constraints[CurrentAWM()].cpu_usage;
	file_logger << std::endl;

	// Count the danger regions and update the output image
	unsigned int close_regions = 0;
	for(unsigned int i_row = 0; i_row < row_number; i_row++) {
		for(unsigned int i_col = 0; i_col < col_number; i_col++)
		{
			if (regions[i_row][i_col] >= static_cast<uint_fast8_t>(ProximityLevel::Near))
				close_regions++; 
		}
	}

	// Handle the state change
	if ( (state == ApplicationState::Normal) && (close_regions > 5) )
	{
		// get the current value of the cp usage
		float cpu_constraint = asrtm->getStaticConstraintValue(0);

		// change the state
		asrtm->changeState("Danger");
		state = ApplicationState::Danger;

		// update the constraint on the cpu usage
		asrtm->changeStaticConstraintGoalValue(0, cpu_constraint);
	}
	if ( (state == ApplicationState::Danger) && (close_regions < 3) )
	{
		// get the current value of the cp usage
		float cpu_constraint = asrtm->getStaticConstraintValue(0);

		// change the state
		asrtm->changeState("Normal");
		state = ApplicationState::Normal;
	
		// update the constraint on the cpu usage
		asrtm->changeStaticConstraintGoalValue(0, cpu_constraint);
	}

	// get the parameters
	bool changed = false;
	params = asrtm->getBestApplicationParameters(&changed);

	if (changed) {
		// update application parameters
		max_hypo_value  = params.at("max_hypo_value");
		hypo_step       = params.at("hypo_step");
		max_arm_length  = params.at("max_arm_length");
		color_threshold = params.at("color_threshold");
		matchcost_limit = params.at("matchcost_limit");
		num_threads     = params.at("nthreads");
#ifdef _OPENMP
		// change the number of OpenMP threads
		if (num_threads > 0) {
			omp_set_num_threads(num_threads);
		}
#endif
		logger->Notice(" - OMP_NUM_THREADS ... %d", num_threads);
		logger->Notice(" - MAX_HYPO_VALUE .... %d", max_hypo_value);
		logger->Notice(" - HYPO_STEP ......... %d", hypo_step);
		logger->Notice(" - MAX_ARM_LENGTH .... %d", max_arm_length);
		logger->Notice(" - COLOR_THRESHOLD ... %d", color_threshold);
		logger->Notice(" - MATCHCOST_LIMIT ... %d", matchcost_limit);
	}

	// check if it's needed to ask for a better awm and print the execution status
	if ( !asrtm->isPossible() && (wmp.awm_id < res_constraints.size() - 1 ) )
	{
		SetGoalGap( static_cast<uint8_t>( throughput_goal->getNAP() * 100 ) );
		status = ExecutionStatus::Starving;
	} else if ( asrtm->isPossible() ) {
		status = ExecutionStatus::Running;
	} else {
		status = ExecutionStatus::Squeezing;
	}

	// write the elaboration results to the buffer
	buffer->setOutput(status, state, regions, outMap);

	return RTLIB_OK;
}

RTLIB_ExitCode_t MviewEXC::onRelease() {

	logger->Info("MviewEXC::onRelease()");

	if (crosses_lx)
		free ((void *) crosses_lx);
	if (crosses_rx)
		free ((void *) crosses_rx);
	if (raw_matchcost)
		free ((void *) raw_matchcost);
	if (matchcost)
		free ((void *) matchcost);
	if (anchorreg)
		free ((void *) anchorreg);
	if (fmatchcost)
		free ((void *) fmatchcost);
	if (fanchorreg)
		free ((void *) fanchorreg);
	if (dpr_votes)
		free ((void *) dpr_votes);
	if (dpr_block)
		free ((void *) dpr_block);

	outMap.release();
	bitmapLx.release();
	bitmapRx.release();
	
	file_logger.close();

	return RTLIB_OK;
}


/* Application-specifc methods */

bool similar(rgb_t p, rgb_t q, int color_threshold)
{
	if (abs( (int)p.B - (int)q.B ) > color_threshold ||
		abs( (int)p.G - (int)q.G ) > color_threshold ||
		abs( (int)p.R - (int)q.R ) > color_threshold)
	{
		return false;
	}
	
	return true;
}

unsigned int MviewEXC::get_square_distance(unsigned int row, unsigned int column )
{
	// var init
	unsigned int summ_distance = 0;
	const unsigned int count_distance = square_size*square_size;

	// loop over the data
	#pragma omp for
	for( unsigned int x = row*square_size; x < (row+1)*square_size; x++ )
	{
		for( unsigned int y =column*square_size; y < (column+1)*square_size; y++)
		{
			summ_distance += outMap.at<uchar>(x,y);
		}
	}

	// summ all the distances
	return summ_distance/count_distance;
}

void MviewEXC::winBuild(cross_t * crosses, const rgb_t *bitmap)
{
	#pragma omp for
	for (int py=0; py<imgheight; py++)
	{
		for (int px=0; px<imgwidth; px++)
		{
			int c;
			const size_t offset = py*imgwidth+px;
			
			rgb_t p = bitmap[offset];
			
			// Move left
			if (px>0)
			{
				c = 1;
				while (px-c>=0 && similar(p, bitmap[py*imgwidth+px-c], color_threshold) && c<=max_arm_length)
				{
					c++;
				}
				
				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].l = c;
			
			// Move right
			if (px<imgwidth-1)
			{
				c = 1;
				while (px+c<imgwidth && similar(p, bitmap[py*imgwidth+px+c], color_threshold) && c<=max_arm_length)
				{
					c++;
				}
				
				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].r = c;
			
			// Move up
			if (py>0)
			{
				c = 1;
				while (py-c>=0 && similar(p, bitmap[(py-c)*imgwidth+px], color_threshold) && c<=max_arm_length)
				{
					c++;
				}
				
				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].u = c;
			
			// Move down
			if (py<imgheight-1)
			{
				c = 1;
				while (py+c<imgheight && similar(p, bitmap[(py+c)*imgwidth+px], color_threshold) && c<=max_arm_length)
				{
					c++;
				}
				
				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].d = c;
		}
	}
}

int pixel_aggr_cost(rgb_t p, rgb_t q, int matchcost_trunc)
{
	int cost = abs((int)p.B-(int)q.B) + abs((int)p.G-(int)q.G) + abs((int)p.R-(int)q.R);
	
	if (cost > matchcost_trunc)
		return matchcost_trunc;
	else
		return cost;
}

void MviewEXC::raw_horizontal_integral(const rgb_t *bitmapLx, const rgb_t *bitmapRx, int disp_cur)
{
	const int xlimit = imgwidth-1-disp_cur;
	
	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			const size_t offset = y*imgwidth+x;
			
			if (x>xlimit)
			{
				raw_matchcost[offset] = raw_matchcost[offset-1] + matchcost_limit;
			}
			else
			{
				int cost = pixel_aggr_cost(bitmapRx[offset], bitmapLx[offset+disp_cur], matchcost_limit);
				
				if (x==0)
				{
					raw_matchcost[offset] = cost;
				}
				else
				{
					raw_matchcost[offset] = raw_matchcost[offset-1] + cost;
				}
			}
		}
	}
}

void get_combined_cross(cross_t a, cross_t b, cross_t *res)
{
	if (a.d<b.d)
		res->d = a.d;
	else
		res->d = b.d;
	
	if (a.l<b.l)
		res->l = a.l;
	else
		res->l = b.l;
	
	if (a.r<b.r)
		res->r = a.r;
	else
		res->r = b.r;
	
	if (a.u<b.u)
		res->u = a.u;
	else
		res->u = b.u;
}

void MviewEXC::horizontal_integral(int disp_cur)
{
	const int xlimit = imgwidth-1-disp_cur;
	
	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			cross_t combined_cross;
			const size_t offset = y*imgwidth+x;
			
			if (x>xlimit)
			{
				combined_cross = crosses_rx[offset];
			}
			else
			{
				get_combined_cross(crosses_rx[offset], crosses_lx[offset+disp_cur], &combined_cross);
			}
			
			anchorreg[offset] = combined_cross.r + combined_cross.l + 1;
			
			if (x==0 || x-combined_cross.l==0)
				matchcost[offset] = raw_matchcost[offset+combined_cross.r];
			else
				matchcost[offset] = raw_matchcost[offset+combined_cross.r] - raw_matchcost[offset-combined_cross.l-1];
		}
	}
}

void MviewEXC::vertical_integral(int disp_cur)
{
	#pragma omp for
	for (int x=0; x<imgwidth; x++)
	{
		for (int y=0; y<imgheight; y++)
		{
			if (y>0)
			{
				matchcost[y*imgwidth+x] += matchcost[(y-1)*imgwidth+x];
				anchorreg[y*imgwidth+x] += anchorreg[(y-1)*imgwidth+x];
			}
		}
	}
}

void MviewEXC::crossregion_integral(int disp_cur)
{
	const int xlimit = imgwidth-1-disp_cur;
	
	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			cross_t combined_cross;
			const size_t offset = y*imgwidth+x;
			
			if (x>xlimit)
			{
				combined_cross = crosses_rx[offset];
			}
			else
			{
				get_combined_cross(crosses_rx[offset], crosses_lx[offset+disp_cur], &combined_cross);
			}
			
			if (y==0 || y-combined_cross.u==0)
			{
				fmatchcost[offset] = matchcost[(y+combined_cross.d)*imgwidth+x];
				fanchorreg[offset] = anchorreg[(y+combined_cross.d)*imgwidth+x];
			}
			else
			{
				fmatchcost[offset] = matchcost[(y+combined_cross.d)*imgwidth+x] - matchcost[(y-combined_cross.u-1)*imgwidth+x];
				fanchorreg[offset] = anchorreg[(y+combined_cross.d)*imgwidth+x] - anchorreg[(y-combined_cross.u)*imgwidth+x];
			}
			
			int costonorm = fmatchcost[offset] / (fanchorreg[offset]+1);
			
			if (disp_cur==0 || costonorm < dpr_votes[offset].costo)
			{
				dpr_votes[offset].costo = costonorm;
				dpr_votes[offset].disparity = (unsigned char) (disp_cur & 0xFF);
			}
		}
	}
}

void MviewEXC::refinement(unsigned char *disparity)
{
	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			size_t offset = y*imgwidth+x;
			
			for (int i=0; i<8; i++)
				dpr_block[offset].bitnum[i] = 0;
			
			for (int h = -crosses_rx[offset].l; h <= crosses_rx[offset].r; h++)
			{
				unsigned char disp = dpr_votes[offset+h].disparity;
				
				for (int i=0; i<8; i++)
					dpr_block[offset].bitnum[i] += (disp >> i) & 0x1;
			}
			
			dpr_block[offset].npixels = crosses_rx[offset].l + crosses_rx[offset].r + 1;
		}
	}
	
	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			size_t offset = y*imgwidth+x;
			
			unsigned int npixels = 0;
			unsigned int bitmap[8] = {0,0,0,0,0,0,0,0};
			
			for (int v = -crosses_rx[offset].u; v <= crosses_rx[offset].d; v++)
			{
				for (int i=0; i<8; i++)
					bitmap[i] += dpr_block[(y+v)*imgwidth+x].bitnum[i];
				
				npixels += dpr_block[(y+v)*imgwidth+x].npixels;
			}
			
			int disp = 0x00;
			for(int i=0; i<8; i++)
				disp |= (npixels < (bitmap[i]<<1)) ? (0x1 << i) : 0x0;
			
			int inty = disp * grayscale;
			if (inty > 255)
				inty = 255;
			disparity[offset] = (inty & 0xFF);
		}
	}
}

