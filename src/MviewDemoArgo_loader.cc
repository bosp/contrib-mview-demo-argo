/**
 *       @file  MviewDemoArgo_exc.cc
 *      @brief  The OpenMP-MView-ARGO BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *              Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <chrono>
#include <unistd.h>

#include "MviewDemoArgo_loader.h"





bool exit_loader = false;


void loadImage(std::string name, ImgBufferPtr buffer, int exc_id)
{
	// Set if the image must be slim
	const bool full_output = false;
	
	// Set the color scalar
	cv::Scalar far = cv::Scalar(95,232,118);
	cv::Scalar near = cv::Scalar(0,255,255);
	cv::Scalar close = cv::Scalar(0,0,255);
	cv::Scalar running = cv::Scalar(255,255,0);
	cv::Scalar starving = cv::Scalar(0,128,255);
	cv::Scalar squeezing = cv::Scalar(255,0,128);
	
	// Set the start point of the proximity regions
	unsigned int x_start = 30;
	unsigned int y_start = 95;
	
	/* Display images */
	cv::Mat output_image;
	cv::Mat display_image;
	cv::Mat left_src;
	cv::Mat right_src;
	cv::Mat status_out;
	cv::Mat disparity_out;
	
	
	/* Stat variables */
	proxRegions regions;
	unsigned int square_size;
	unsigned int col_number;
	unsigned int row_number;
	
	/* Application stat */
	ApplicationState state;
	ExecutionStatus status;
	
	/* Frame size */
	int imgwidth;
	int imgheight;
	
	// Get the size of the images
	int imgtype;
	buffer->getImageStat(imgwidth, imgheight, imgtype);	
	
	// Get the size and dimension proximity regions
	buffer->getProximityRegionStat(square_size, row_number, col_number);
	
	// Final ratio to display !!!!
	square_size /= 2;
	
	// Create the output image
	output_image = cv::Mat(imgheight*2, imgwidth*2, imgtype, cv::Scalar(0,0,0));
	
	// obtain the roi region of the left img
	cv::Rect region_of_interest = cv::Rect(0, 0, imgwidth, imgheight);
	left_src = output_image(region_of_interest);
	
	
	// obtain the roi region of the right img
	region_of_interest = cv::Rect(imgwidth, 0, imgwidth, imgheight);
	right_src = output_image(region_of_interest);
	
	
	// obtain the roi region of the output map
	region_of_interest = cv::Rect(imgwidth, imgheight, imgwidth, imgheight );
	disparity_out = output_image(region_of_interest);
	
	
	// copy the reference image
	region_of_interest = cv::Rect(0, imgheight, imgwidth, imgheight );
	status_out = output_image(region_of_interest);
	
	// get the display image
	if (full_output)
		region_of_interest = cv::Rect(0, 0, imgwidth*2, imgheight*2);
	else
		region_of_interest = cv::Rect(0, 0, imgwidth, imgheight*2);
	display_image = output_image(region_of_interest);
	
	// get the input images
	buffer->getInputImages(left_src, right_src);
	
	// get the application output
	buffer->getOutput(status, state, regions, disparity_out);
	
	
	// Fill the status map with black
	status_out.setTo(cv::Scalar(0,0,0));
	
	
	/* Constraint status:
	 * Green  -> all constraint are stasfied
	 * Yellow -> at least one constraint is not satisfied, but
	 *           is possible to get additional resources
	 * Red    -> at least one constraint is not satisfied, and
	 *           is not possible to get additional resources
	 */
	cv::putText(status_out, "Execution status", cv::Point(10,30), CV_FONT_HERSHEY_SCRIPT_SIMPLEX, 0.8, cv::Scalar(255,255,255), 1, CV_AA );
	
	// Build the alternate reference framework setup
	cv::putText(status_out, "Proximity level", cv::Point(45, 80), CV_FONT_HERSHEY_SCRIPT_SIMPLEX, 0.8, cv::Scalar(255,255,255), 1, CV_AA );
	
	// Put a line in order to separate the image zones
	const unsigned int line_offset = 10;
	cv::line(status_out, cv::Point(line_offset,50), cv::Point(imgwidth-line_offset, 50), cv::Scalar(100,100,100), 2);
	
	// Put the legend about the closeness
	cv::putText(status_out, "Far", cv::Point(30, imgheight - 30), CV_FONT_HERSHEY_PLAIN, 1.2, far, 1, CV_AA );
	cv::putText(status_out, "Near", cv::Point(100, imgheight - 30), CV_FONT_HERSHEY_PLAIN, 1.2, near, 1, CV_AA );
	cv::putText(status_out, "Close", cv::Point(180, imgheight - 30), CV_FONT_HERSHEY_PLAIN, 1.2, close, 1, CV_AA );
	
	// Put the legend about the execution status
	cv::putText(status_out, "Running", cv::Point(200, 40), CV_FONT_HERSHEY_PLAIN, 0.8, running, 1, CV_AA );
	cv::putText(status_out, "Starving", cv::Point(270, 40), CV_FONT_HERSHEY_PLAIN, 0.8, starving, 1, CV_AA );
	cv::putText(status_out, "Squeezing", cv::Point(340, 40), CV_FONT_HERSHEY_PLAIN, 0.8, squeezing, 1, CV_AA );
	
	// Put the legend about the application states
	cv::putText(status_out, "Normal", cv::Point(320, 200), CV_FONT_HERSHEY_PLAIN, 1.2, far, 1, CV_AA );
	cv::putText(status_out, "Danger", cv::Point(320, 300), CV_FONT_HERSHEY_PLAIN, 1.2, close, 1, CV_AA );
	
	
	
	// Put a line to isolate the state
	cv::line(status_out, cv::Point(280, 50), cv::Point(280, imgheight - line_offset), cv::Scalar(100,100,100), 2);
	
	// Print the current state header
	cv::putText(status_out, "Application", cv::Point(295, 80), CV_FONT_HERSHEY_SCRIPT_SIMPLEX, 0.8, cv::Scalar(255,255,255), 1, CV_AA );
	
	// Print the current state header
	cv::putText(status_out, "state", cv::Point(330, 105), CV_FONT_HERSHEY_SCRIPT_SIMPLEX, 0.8, cv::Scalar(255,255,255), 1, CV_AA );
	
	
	
	// Draw the status panel informations
	// The state points
	cv::Point state1_p = cv::Point(350, 150);
	cv::Point state2_p = cv::Point(350, 250);
	unsigned int state_radius = 30;
	
	// Refill the circles with black
	cv::circle(status_out, state1_p, state_radius, cv::Scalar(0,0,0), CV_FILLED, 1);
	cv::circle(status_out, state2_p, state_radius, cv::Scalar(0,0,0), CV_FILLED, 1);
	
	// Based on the application state fill the circles
	switch (state)
	{
		case ApplicationState::Normal:
			cv::circle(status_out, state1_p, state_radius, far, CV_FILLED, 1);
			cv::circle(status_out, state2_p, state_radius, close, 1, 1);
			break;
		case ApplicationState::Danger:
			cv::circle(status_out, state1_p, state_radius, far, 1, 1);
			cv::circle(status_out, state2_p, state_radius, close, CV_FILLED, 1);
			break;
			
		default:
			cv::circle(status_out, state1_p, state_radius, far, 1, 1);
			cv::circle(status_out, state2_p, state_radius, close, 1, 1);
			break;
	}
	
	// The three points
	cv::Point normal_status_p = cv::Point(225,15);
	cv::Point req_res_status_p = cv::Point(295,15);
	cv::Point imp_status_p = cv::Point(370,15);
	
	// Refill the circles with black
	cv::circle(status_out, normal_status_p, 10, cv::Scalar(0,0,0), CV_FILLED, 1);
	cv::circle(status_out, req_res_status_p, 10, cv::Scalar(0,0,0), CV_FILLED, 1);
	cv::circle(status_out, imp_status_p, 10, cv::Scalar(0,0,0), CV_FILLED, 1);
	
	// Based on the status change the fills
	switch (status)
	{
		case ExecutionStatus::Running:
			cv::circle(status_out, normal_status_p, 10, running, CV_FILLED, 1);
			cv::circle(status_out, req_res_status_p, 10, starving,  1, 1);
			cv::circle(status_out, imp_status_p, 10, squeezing, 1, 1);
			break;
		case ExecutionStatus::Starving:
			cv::circle(status_out, normal_status_p, 10, running, 1, 1);
			cv::circle(status_out, req_res_status_p, 10, starving,  CV_FILLED, 1);
			cv::circle(status_out, imp_status_p, 10, squeezing, 1, 1);
			break;
		case ExecutionStatus::Squeezing:
			cv::circle(status_out, normal_status_p, 10, running, 1, 1);
			cv::circle(status_out, req_res_status_p, 10, starving,  1, 1);
			cv::circle(status_out, imp_status_p, 10, squeezing, CV_FILLED, 1);
			break;
			
		default:
			cv::circle(status_out, normal_status_p, 10, running, 1, 1);
			cv::circle(status_out, req_res_status_p, 10, starving,  1, 1);
			cv::circle(status_out, imp_status_p, 10, squeezing, 1, 1);
	}
	
	for( unsigned int row = 0; row < row_number; row++ )
	{
		for( unsigned int column = 0; column < col_number; column++ )
		{
			// compute the coordinates of the center point
			unsigned int x_circle = x_start + (column)*square_size + (square_size)/2;
			unsigned int y_circle = y_start + (row)*square_size + (square_size)/2;
			
			// find th color of the circle
			cv::Scalar color;
			if ((regions[row][column] >= 0) && (regions[row][column] < static_cast<uint_fast8_t>(ProximityLevel::Close))) {
				color = far;
			} else if ((regions[row][column] >= static_cast<uint_fast8_t>(ProximityLevel::Close)) && (regions[row][column] < static_cast<uint_fast8_t>(ProximityLevel::Near))) {
				color = near;
			} else {
				color = close;
			}
			
			// print the circle
			cv::circle( status_out, cv::Point(x_circle, y_circle), square_size/2, color, CV_FILLED );
			
		}
	}
	
	// display the image
	std::string window_name = "MviewDemoArgo - " + std::to_string(exc_id);
	cv::imshow(window_name, display_image);
	cv::waitKey(100);
	
	
	unsigned int fps = 10;
	
	while(!exit_loader)
	{
		// get the now istant
		std::chrono::steady_clock::time_point time = std::chrono::steady_clock::now();
		
		// get the input images
		buffer->getInputImages(left_src, right_src);
		
		
		// get the output images
		buffer->getOutput(status, state, regions, disparity_out);
		
		
		// print the state
		// The state points
		cv::Point state1_p = cv::Point(350, 150);
		cv::Point state2_p = cv::Point(350, 250);
		unsigned int state_radius = 30;
		
		// Refill the circles with black
		cv::circle(status_out, state1_p, state_radius, cv::Scalar(0,0,0), CV_FILLED, 1);
		cv::circle(status_out, state2_p, state_radius, cv::Scalar(0,0,0), CV_FILLED, 1);
		
		// Based on the application state fill the circles
		switch (state)
		{
			case ApplicationState::Normal:
				cv::circle(status_out, state1_p, state_radius, far, CV_FILLED, 1);
				cv::circle(status_out, state2_p, state_radius, close, 1, 1);
				break;
			case ApplicationState::Danger:
				cv::circle(status_out, state1_p, state_radius, far, 1, 1);
				cv::circle(status_out, state2_p, state_radius, close, CV_FILLED, 1);
				break;
				
			default:
				cv::circle(status_out, state1_p, state_radius, far, 1, 1);
				cv::circle(status_out, state2_p, state_radius, close, 1, 1);
				break;
		}
		
		
		// The three points
		cv::Point normal_status_p = cv::Point(225,15);
		cv::Point req_res_status_p = cv::Point(295,15);
		cv::Point imp_status_p = cv::Point(370,15);
		
		// Refill the circles with black
		cv::circle(status_out, normal_status_p, 10, cv::Scalar(0,0,0), CV_FILLED, 1);
		cv::circle(status_out, req_res_status_p, 10, cv::Scalar(0,0,0), CV_FILLED, 1);
		cv::circle(status_out, imp_status_p, 10, cv::Scalar(0,0,0), CV_FILLED, 1);
		
		// Based on the status change the fills
		switch (status)
		{
			case ExecutionStatus::Running:
				cv::circle(status_out, normal_status_p, 10, running, CV_FILLED, 1);
				cv::circle(status_out, req_res_status_p, 10, starving,  1, 1);
				cv::circle(status_out, imp_status_p, 10, squeezing, 1, 1);
				break;
			case ExecutionStatus::Starving:
				cv::circle(status_out, normal_status_p, 10, running, 1, 1);
				cv::circle(status_out, req_res_status_p, 10, starving,  CV_FILLED, 1);
				cv::circle(status_out, imp_status_p, 10, squeezing, 1, 1);
				break;
			case ExecutionStatus::Squeezing:
				cv::circle(status_out, normal_status_p, 10, running, 1, 1);
				cv::circle(status_out, req_res_status_p, 10, starving,  1, 1);
				cv::circle(status_out, imp_status_p, 10, squeezing, CV_FILLED, 1);
				break;
				
			default:
				cv::circle(status_out, normal_status_p, 10, running, 1, 1);
				cv::circle(status_out, req_res_status_p, 10, starving,  1, 1);
				cv::circle(status_out, imp_status_p, 10, squeezing, 1, 1);
		}
		
		for( unsigned int row = 0; row < row_number; row++ )
		{
			for( unsigned int column = 0; column < col_number; column++ )
			{
				// compute the coordinates of the center point
				unsigned int x_circle = x_start + (column)*square_size + (square_size)/2;
				unsigned int y_circle = y_start + (row)*square_size + (square_size)/2;
				
				// find th color of the circle
				cv::Scalar color;
				if ((regions[row][column] >= 0) && (regions[row][column] < static_cast<uint_fast8_t>(ProximityLevel::Close))) {
					color = far;
				} else if ((regions[row][column] >= static_cast<uint_fast8_t>(ProximityLevel::Close)) && (regions[row][column] < static_cast<uint_fast8_t>(ProximityLevel::Near))) {
					color = near;
				} else {
					color = close;
				}
				
				// print the circle
				cv::circle( status_out, cv::Point(x_circle, y_circle), square_size/2, color, CV_FILLED );
				
			}
		}
		
		
		// update the image
		cv::imshow("MviewDemoArgo - " + std::to_string(exc_id), display_image);
		cv::waitKey(1);
		
		
		// get the next image
		buffer->next();
		
		
		// get the elapsed time
		unsigned int elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::steady_clock::now() - time
		).count();
		
		// compute the time to sleep
		int slack_time_ms = static_cast<int>(1000/fps) - static_cast<int>(elapsed_time);
		
		// sleep
		if (slack_time_ms > 0)
			usleep(slack_time_ms*1000);
	}

}
