#!/usr/bin/env python

# import the xml parser
from xml.dom import minidom

# standard import
import os, sys
import argparse


class OperatingPointParser(object):


	def __init__(self, path_xml_file_op):

		# save the root of the file
		xmldoc = minidom.parse( path_xml_file_op )

		# define the list of operating point
		self.ops = []

		# get the list of xml ops
		xml_ops = xmldoc.getElementsByTagName('point')

		# loop over the xml op elements
		for xml_op in xml_ops:

			# get the parameters
			xml_params = xml_op.getElementsByTagName('parameter')

			# get the metrics
			xml_metrics = xml_op.getElementsByTagName('system_metric')

			# defines the dictionaries of metric and parameters
			params = {}
			metrics = {}

			# populate the parameters
			for param in xml_params:

				# insert the value
				params[param.attributes['name'].value] = float(param.attributes['value'].value)


			# populate the metrics
			for metric in xml_metrics:

				# insert the value
				metrics[metric.attributes['name'].value] = float(metric.attributes['value'].value)


			# insert the operating point
			self.ops.append( [dict(params), dict(metrics)] )



	def print_op( self, metric_insterested, parameter ):

		# declare the dict of data (one for awm)
		op_lists = {}

		# declare the list of colors
		colors = [
			'#a6cee3',
			'#1f78b4',
			'#b2df8a',
			'#33a02c',
			'#fb9a99',
			'#e31a1c',
			'#fdbf6f',
			'#ff7f00',
			'#cab2d6',
			'#6a3d9a',
			'#ffff99',
			'#b15928'
		]


		# loop over the ops
		for op in self.ops:

			# get the parameter value
			param = float(op[0][parameter])

			# get the point
			x = float(op[1][metric_insterested[0]])
			y = float(op[1][metric_insterested[1]])

			# add to the dict
			if param in op_lists.keys():
				op_lists[param][0].append(x)
				op_lists[param][1].append(y)
			else:
				op_lists[param] = [ [ x ], [ y ] ]


		# import the matplot library
		import matplotlib.pyplot as plt

		# plot the data
		fig = plt.figure()
		fig.canvas.set_window_title('Op List')
		ax1 = fig.add_subplot(111)

		# set title and axis
		plt.title('Operating Points grouped by ' + str(parameter))
		plt.xlabel(metric_insterested[0])
		plt.ylabel(metric_insterested[1])


		# for every parameters
		color_index  = 0
		for param in op_lists:

			# plot the data
			ax1.scatter(op_lists[param][0], op_lists[param][1], label = str(int(param)), color = colors[color_index], edgecolor='black', alpha=0.6)

			# decrement the color index
			color_index += 1


		# set the legend
		plt.legend(loc='upper left');

		# Remove top and right axes lines ("spines")
		spines_to_remove = ['top', 'right']
		for spine in spines_to_remove:
			ax1.spines[spine].set_visible(False)


		# Get rid of ticks. The position of the numbers is informative enough of
		# the position of the value.
		ax1.xaxis.set_ticks_position('none')
		ax1.yaxis.set_ticks_position('none')	

		# show the plot
		plt.show()








	def __str__( self ):

		# write the header of the list
		out_str  = "\n\n// The Operating Point List\n"
		out_str += "argo::asrtm::OperatingPointsList opList = {\n"


		# loop over the ops
		counter = 0
		for op in self.ops:

			# write the op header
			out_str += "  { // Operating Point " + str(counter) +"\n"

			# loop over the parameters
			out_str += "    {  // Parameters\n"
			for p in op[0]:
				# write the param
				out_str += "      { \"" + str(p) + "\", " + str(op[0][p]) + "f },\n"
			out_str += "    },\n"


			# loop over the metrics
			out_str += "    {  // Metrics\n"
			for m in op[1]:
				# write the metric
				out_str += "      { \"" + str(m) + "\", " + str(op[1][m]) + "f },\n"
			out_str += "    },\n"

			# loop over the states
			out_str += "    {  // States\n"
			try:
				for s in self.states:
					out_str += "      \"" + str(s) + "\", \n"
			except AttributeError:
				out_str += "      \"Default\"\n"
			out_str += "    }\n"

			# increment the op counter
			counter += 1

			# write the op tail
			out_str += "  },\n"

		# write the tail of the list
		out_str += "};\n"

		return out_str






class ResourceParser(object):

	def __init__( self, path_xml_file_awm):

		# get the root of the xml file
		xmldoc = minidom.parse( path_xml_file_awm )

		# define the needed resources to put constraints
		self.res = {
			'pe' : 'cpu_usage',
			'mem': 'memory'
		}

		# declare the awm constraints
		self.awm_constraint = {}

		# try if it works
		awm_list  = xmldoc.getElementsByTagName('awm')

		# loop over the awms
		for awm in awm_list:

			# get the awm resource container
			resources_container = awm.getElementsByTagName('resources')

			# declare the constraints dictionary
			res_constraint = {}

			# loop over the interested constraints
			for key in self.res:

				# get the constraint
				constraint = resources_container[0].getElementsByTagName(key)

				# add to the list of constraint id needed
				res_constraint[self.res[key]] = float(constraint[0].attributes['qty'].value)

			# save the constraints
			self.awm_constraint[int(awm.attributes['id'].value)] = dict(res_constraint)





	def normalize_cpu_quota( self ):

		# for all the awm
		for awm in self.awm_constraint:
			# check if the cpu_quota is present
			if 'cpu_usage' in self.awm_constraint[awm]:
				self.awm_constraint[awm]['cpu_usage'] /= 100



	def struct_header(self):

		out_str = "typedef struct bbque_resource {\n"

		# loop over the resources
		for res_name in self.res:
			out_str += "  float " + self.res[res_name] + ";\n"

		out_str += "} bbque_resource_t;\n"

		return out_str




	def __str__( self ):

		# write the header
		out_string  = "\n\n//Barbeque awm constraint values\n"
		out_string += "const std::vector<bbque_resource_t> res_constraints = {\n"

		# loop over the awms
		for awm_id in range(0, len(self.awm_constraint)):

			# write the row header
			out_string += "  { "

			# loop over the resource limit
			for res_name in self.res:

				# write the number
				out_string += "/* " + self.res[res_name] + " */  " + str(self.awm_constraint[awm_id][self.res[res_name]]) + "f,        "

			# write the row tail
			out_string += "},\n"


		# write the list tail
		out_string += "};\n\n"

		return out_string



def get_realpath_filename( name ):

	# check if the name is correct
	if (os.path.isfile(name)):
		return os.path.realpath(name)

	# check if it needs to add the ./
	name1 = "./" + name
	if (os.path.isfile(name1)):
		return os.path.realpath(name1)

	# check if it is relative to this file
	name2 = os.path.join(os.path.dirname(__file__), name)
	if (os.path.isfile(name2)):
		return os.path.realpath(name2)


	# exit with an error
	sys.exit( "Can't find the file " + name )






if __name__ == "__main__":

	########################################
	## Option parsing
	########################################

	# Create the arguments parser
	parser = argparse.ArgumentParser(description='Parse the bbq recipe and ops xml files in order to build the needed c src files')

	# add the arguments
	parser.add_argument('-r','--recipe', '--path_bbque_recipe', type=str, required = True,
                   help='Path to the bbque recipe', dest ="path_xml_awm", metavar='path_recipe')

	parser.add_argument('-o','--ops','--path_op_list', type=str, required = True,
                   help='Path to the Operating Point\'s List xml file', dest ="path_xml_ops", metavar='path_op_list')
	parser.add_argument('-d','--display', type=bool, required = False,
                   help='Display the Operating points grouped by Threads number', dest ="display_op", metavar='display', default=False)
	parser.add_argument('-p','--base_path', type=str, required = False,
		   help='The base path for the src/ and include/ files', dest = "base_path", metavar='base_path', 
		   default=os.path.realpath(os.path.dirname(__file__)))


	# parse the input vars
	args = parser.parse_args()




	########################################
	## Defining the path of the out files
	########################################

	# define the path of the output files
	path_cc = os.path.realpath(os.path.join(args.base_path, "src/op_list.cc" ))
	path_h = os.path.realpath(os.path.join(args.base_path, "include/op_list.h" ))


	# checking the path retrived from the input
	path_xml_awm = get_realpath_filename( str(args.path_xml_awm) )
	path_xml_ops = get_realpath_filename( str(args.path_xml_ops) )



	########################################
	## Parse the xml files
	########################################
	

	# parse the awms
	awm_list = ResourceParser( path_xml_awm )

	# normalize its cpu quota value
	awm_list.normalize_cpu_quota()

	# parse the ops
	op_list = OperatingPointParser( path_xml_ops )

	# insert the states
	op_list.states = [ 'Normal', 'Danger' ]



	########################################
	## Writing the header
	########################################

	try:
		header_file = open(path_h, "w")
		header_file.write("#ifndef AUTO_OP_LIST_H\n")
		header_file.write("#define AUTO_OP_LIST_H\n")
		header_file.write('#include <argo/asrtm/operating_point.h>\n')
		header_file.write("\n\n\n/* WARNING: Autogenerated_file! */\n")
		header_file.write('\n\n\n// Barbeque awm_constraint structure\n')
		header_file.write(awm_list.struct_header())
		header_file.write('\n\n\n// Awm constraints vector\n')
		header_file.write('extern const std::vector<bbque_resource_t> res_constraints;\n')
		header_file.write('\n\n\n// Operating Point Lists\n')
		header_file.write('extern argo::asrtm::OperatingPointsList opList;\n')
		header_file.write('\n\n\n#endif // AUTO_OP_LIST_H\n\n')
		header_file.close()
	except IOError:
		header_file.close()
		sys.exit("Error on generating the header file!")




	########################################
	## Writing the source file
	########################################

	try:
		src_file = open(path_cc, "w")
		src_file.write("#include <op_list.h>\n")
		src_file.write("\n\n\n/* WARNING: Autogenerated_file! */\n")
		src_file.write(str(awm_list))
		src_file.write(str(op_list))
		src_file.close()
	except IOError:
		header_file.close()
		sys.exit("Error on generating the src file!")



	########################################
	## Show the results
	########################################

	if bool(args.display_op):
		op_list.print_op(["norm_error", "throughput"], "nthreads")








