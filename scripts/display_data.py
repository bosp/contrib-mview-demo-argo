#!/usr/bin/env python
import os,sys

# FIXME workarounf for ssh sessions
#import matplotlib
#matplotlib.use('GTK')


from matplotlib.pylab import subplots,close
from matplotlib import cm
from matplotlib import pyplot as plt
import matplotlib.animation as animation


import numpy as np

import argparse


class States(object):
	ready = 1
	parsing = 3


class LogDisplayer(object):


	def __init__( self, log_file, plot_layout, y_limits, plot_titles = None, timestamp = "Timestamp", iid = 1 ):

		# copy the layouts
		self.plot_layout = plot_layout
		self.timestap_layout = timestamp

		# creating the timestamp
		self.timestamp = []
		self.timestamp_multiplier = 1.0/1000000.0


		# initialize the attributes
		self.data = {}
		self.max = {}
		self.lines = {}
		self.columns = {}
		self.legends = []
		self.y_limits = [ limit for limit in y_limits ]


		# initial state
		self.state = States.ready


		# open the file
		self.log_file = log_file


		# create the container
		light_grey = np.array([float(248)/float(255)]*3)
		self.figure, self.axarr = subplots( len(self.plot_layout), sharex = True, figsize = (10,10) )

		# loop over the plots
		index_axaarr = 0
		for key in self.plot_layout:

			# loop over the data series
			for metric_name in self.plot_layout[key]:

				# add the data field
				self.data[metric_name] = []

				# plot the data series and save the line
				self.lines[metric_name], = self.axarr[index_axaarr].plot(
					self.timestamp,
					self.data[metric_name],
					label = metric_name,
					linewidth=2.0,
					markersize=5,
					marker = 3
					)


			# set the y_label
			self.axarr[index_axaarr].set_ylabel( key )

			# set the y_limit (blit bug)
			self.axarr[index_axaarr].set_ylim([0, self.y_limits[index_axaarr]])

			# set the x_label only on the lower graph
			if index_axaarr == len(self.axarr) - 1:
				self.axarr[index_axaarr].set_xlabel( "Time Window [20s]" )

			# set the legend
			legend = self.axarr[index_axaarr].legend( loc = 'upper right', ncol = len(self.plot_layout[key]), prop=dict(size='small'))
			self.legends.append( legend )

			# set the title
			if plot_titles:
				self.axarr[index_axaarr].set_title(plot_titles[index_axaarr])


			# update the index of the plot
			index_axaarr += 1


		# save the iid
		self.iid = str(iid)


		# apply cosmetics improvement
		self.beautify_plot( )



	def beautify_plot( self ):

		# set the window name
		self.figure.canvas.set_window_title("MviewDemoArgo" + self.iid + " Run-Time Profile") 

		# set the fonts
		plt.rcParams.update({'font.size': 14, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})

		# set the tight layout
		plt.tight_layout()

		# loop over all the axes
		for index in range(0, len(self.axarr)):

			# get the lengend frame
			rect = self.legends[index].get_frame()

			# set the color
			light_grey = np.array([float(248)/float(255)]*3)
			rect.set_facecolor(light_grey)
			rect.set_linewidth(0.3)

			# set the grid
			self.axarr[index].grid(True)


		# actually this a bug fix for blit option
		plt.tick_params(
			axis='x',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			top='off',         # ticks along the top edge are off
			labelbottom='off'  # labels along the bottom edge are off
			) 




	def parse_log_header( self, row):

		# chomp the newline
		row = row.rstrip('\n')

		# parse the row
		counter = 0
		for word in row.split(", "):

			# save the column name
			self.columns[word] = counter

			# update the counter
			counter += 1

		# change the state
		self.state = States.parsing




	def parse_profile_row( self, row):

		# chomp the newline
		row = row.rstrip('\n')

		# split the parsed row
		new_datas = row.split(", ")

		# update the new data
		for key in self.plot_layout:

			# loop over the interested metrics
			for metric_name in self.plot_layout[key]:

				# get the new data
				new_data = float(new_datas[self.columns[metric_name]])

				# append the new value
				self.data[metric_name].append( new_data )

				# check for the maximum
				try:
					if self.max[key] < new_data:
						self.max[key] = new_data
				except KeyError:
					self.max[key] = new_data


		# get the new timestamp
		new_timestamp = float(new_datas[self.columns[self.timestap_layout]])

		# adjust its value
		new_timestamp *= self.timestamp_multiplier


		# remove the offset
		try:
			new_timestamp -= self.first_timestamp
		except AttributeError:
			self.first_timestamp = new_timestamp
			new_timestamp = 0.0


		# append the timestamp
		self.timestamp.append( new_timestamp )



	def parse( self ):

		redraw = False
		new_row = True

		# loop over the log file
		while(new_row):
			# read a new_row
			new_row = self.log_file.readline()

			# check if the row is not
			if not new_row:
				break

			# if we are in the ready state
			if self.state == States.ready:

				# parse the header
				self.parse_log_header( new_row )

				# read next line
				continue


			# if we are in the parsing state
			if self.state == States.parsing:

				# parse the new data
				self.parse_profile_row( new_row )

				# set redrowing to true
				redraw = True

		return redraw




	def update( self, what ):

		# check if there is something new
		redrowing = self.parse()

		# if not return without redrawing anything
		if not redrowing:
			return []

		# update the data lines
		for key in self.lines:

			# update with the new data
			self.lines[key].set_data(self.timestamp, self.data[key])


		# update the axis
		count = 0
		for key in self.plot_layout:

			# rescale the time-limit
			try:
				if self.timestamp[-1] - 20  < 0:
					lower_lim = 0
				else:
					lower_lim = self.timestamp[-1] - 20

				upper_lim = max(self.timestamp[-1], 20)
			except IndexError:
				lower_lim = 0
				upper_lim = 20
			

			self.axarr[count].set_xlim([lower_lim, upper_lim])

			# compute the y-limit
			#limit_value = self.max[key] + self.max[key]*0.28
			#self.axarr[count].set_ylim([0, limit_value])
			count += 1


		# return to the ui main loop
		return [ ax for ax in self.axarr ]



	def play( self ):

		# add the animation to the image
		ani = animation.FuncAnimation(self.figure, self.update, interval=500, blit=True)

		# show the image
		plt.show( )

		# at the end close the file
		self.log_file.close()




def display_log( log_file_name, plot_layout, y_limits, plot_titles = None, timestamp = "Timestamp", iid = 1 ):

	# check if the file
	try:
		log_file = open( log_file_name , "r")
	except IOError:
		# touch the file
		os.system( "touch " + log_file_name )

		# now open the file
		log_file = open( log_file_name , "r")


	# create the object
	argoDisplayer = LogDisplayer( log_file= log_file,
		 plot_layout = plot_layout,
		 plot_titles = plot_titles,
		 timestamp = "Timestamp",
		 y_limits = y_limits,
		 iid = iid
		 )

	# display the data
	argoDisplayer.play()









if __name__ == "__main__":


	# create the arg parser
	parser = argparse.ArgumentParser(description='Display at Run-Time the log file data')

	# add the option
	parser.add_argument( '-l','--log_file', help='The path of the log file', dest = 'log_file_path', required = True )
	parser.add_argument( '-i','--instance_id', help='The instance identifier', dest = 'iid', required = False, default = 1 )


	# parse the arguments
	args = parser.parse_args()

	# create the plot layout ( y_label -> [ metric_name_from_log ]] )
	plot_layout = {
		"Throughput [fps]": ["Throughput_profiled", "Throughput_measured", "Throughput_goal"],
		"Error [%]": ["Error_profiled", "Error_goal"],
		"Cpu Usage [%]": ["Cpu_usage", "Cpu_available"]
		}

	plot_titles = [ "Throughput", "Normalized error", "Cpu usage" ]


	y_limits = [13, 1.2, 7.5]



	# plot the log
	display_log( log_file_name = args.log_file_path,
		 plot_layout = plot_layout,
		 plot_titles = plot_titles,
		 timestamp = "Timestamp",
		 y_limits = y_limits,
		 iid = args.iid
		 )


	
