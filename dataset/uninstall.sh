#!/bin/bash

DATASET=( "Baby1" "Baby3" "Bowling2" "Cloth2" "Cloth4" "Lampshade2" "Midd2" "Plastic" "Rocks2" "Wood2" "Aloe" "Baby2" "Bowling1" "Cloth1" "Cloth3" "Flowerpots" "Lampshade1" "Midd1" "Monopoly" "Rocks1" "Wood1" )

for dir in ${DATASET[@]}
do
	rm -rf $dir
done

exit 0
